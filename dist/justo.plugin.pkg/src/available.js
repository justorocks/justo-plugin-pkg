"use strict";

var _dogmalang = require("dogmalang");

function available(params, ssh) {
  let res = false;_dogmalang.dogma.paramExpectedToHave("params", params, { pkg: { type: _dogmalang.text, mandatory: true } });_dogmalang.dogma.paramExpectedToBe("ssh", ssh, _dogmalang.func);{
    const $aux15111791611708 = (((params != null ? params.target : null) != null ? (params != null ? params.target : null).os : null) != null ? ((params != null ? params.target : null) != null ? (params != null ? params.target : null).os : null).id : null) || _dogmalang.os.release().id;if ($aux15111791611708 == "alpine") {
      res = apk(params, ssh);
    } else if ($aux15111791611708 == "ubuntu") {
      res = apt(params, ssh);
    } else {
      _dogmalang.dogma.raise("OS not supported.");
    }
  }return res;
}module.exports = exports = available;
function apk(params, ssh) {
  _dogmalang.dogma.paramExpected("params", params, null);{
    return _dogmalang.dogma.like((0, _dogmalang.text)((ssh || _dogmalang.exec)("apk search -x " + params.pkg)), (0, _dogmalang.fmt)("\\b%s\\b", params.pkg));
  }
}
function apt(params, ssh) {
  _dogmalang.dogma.paramExpected("params", params, null);{
    return _dogmalang.dogma.getItem(_dogmalang.dogma.peval(() => {
      return (ssh || _dogmalang.exec)((0, _dogmalang.fmt)("apt show %s 2> /dev/null", params.pkg));
    }), 0);
  }
}