"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.available = exports.installed = exports.remove = exports.del = exports.install = exports.add = exports.update = undefined;

var _dogmalang = require("dogmalang");

var _justo = require("justo");

const update = (0, _justo.simple)({ ["id"]: "update", ["desc"]: "Update local index.", ["title"]: "Update local index" }, _dogmalang.dogma.use(require("./update")));exports.update = update;

const add = (0, _justo.simple)({ ["id"]: "add", ["desc"]: "Install one or multiple packages.", ["fmt"]: params => {
    _dogmalang.dogma.paramExpected("params", params, null);{
      return (0, _dogmalang.fmt)("Install %s", (0, _dogmalang.list)(params.pkg).join(", "));
    }
  } }, _dogmalang.dogma.use(require("./add")));exports.add = add;

const install = add;exports.install = install;

const del = (0, _justo.simple)({ ["id"]: "del", ["desc"]: "Remove one or multiple packages.", ["fmt"]: params => {
    _dogmalang.dogma.paramExpected("params", params, null);{
      return (0, _dogmalang.fmt)("Remove %s", (0, _dogmalang.list)(params.pkg).join(", "));
    }
  } }, _dogmalang.dogma.use(require("./del")));exports.del = del;

const remove = del;exports.remove = remove;

const installed = (0, _justo.simple)({ ["id"]: "installed", ["desc"]: "Check whether a package is installed.", ["fmt"]: params => {
    _dogmalang.dogma.paramExpected("params", params, null);{
      return (0, _dogmalang.fmt)("Check if %s installed", params.pkg);
    }
  } }, _dogmalang.dogma.use(require("./installed")));exports.installed = installed;

const available = (0, _justo.simple)({ ["id"]: "available", ["desc"]: "Check if a package available to install.", ["fmt"]: params => {
    _dogmalang.dogma.paramExpected("params", params, null);{
      return (0, _dogmalang.fmt)("Check if %s available", params.pkg);
    }
  } }, _dogmalang.dogma.use(require("./available")));exports.available = available;