"use strict";

var _dogmalang = require("dogmalang");

function installed(params, ssh, log) {
  let res = false;_dogmalang.dogma.paramExpectedToHave("params", params, { pkg: { type: _dogmalang.text, mandatory: true } });_dogmalang.dogma.paramExpectedToBe("ssh", ssh, _dogmalang.func);_dogmalang.dogma.paramExpected("log", log, null);{
    const $aux15111791618513 = (((params != null ? params.target : null) != null ? (params != null ? params.target : null).os : null) != null ? ((params != null ? params.target : null) != null ? (params != null ? params.target : null).os : null).id : null) || _dogmalang.os.release().id;if ($aux15111791618513 == "alpine") {
      res = apk(params, ssh);
    } else if ($aux15111791618513 == "ubuntu") {
      res = apt(params, ssh);
    } else {
      _dogmalang.dogma.raise("OS not supported.");
    }
  }return res;
}module.exports = exports = installed;
function apk(params, ssh) {
  _dogmalang.dogma.paramExpected("params", params, null);{
    return _dogmalang.dogma.like((0, _dogmalang.text)((ssh || _dogmalang.exec)("apk info -e " + params.pkg)), (0, _dogmalang.fmt)("\\b%s\\b", params.pkg));
  }
}
function apt(params, ssh) {
  _dogmalang.dogma.paramExpected("params", params, null);{
    return _dogmalang.dogma.includes((0, _dogmalang.text)((ssh || _dogmalang.exec)((0, _dogmalang.fmt)("apt list --installed %s 2> /dev/null", params.pkg))), params.pkg);
  }
}