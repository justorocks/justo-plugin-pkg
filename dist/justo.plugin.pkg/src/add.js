"use strict";

var _dogmalang = require("dogmalang");

function add(params, ssh) {
  _dogmalang.dogma.paramExpectedToHave("params", params, { pkg: { type: [_dogmalang.list, _dogmalang.text], mandatory: true }, reinstall: { type: _dogmalang.bool, mandatory: false }, allowUntrusted: { type: _dogmalang.bool, mandatory: false }, allowUnauthenticated: { type: _dogmalang.bool, mandatory: false } });_dogmalang.dogma.paramExpectedToBe("ssh", ssh, _dogmalang.func);params.pkg = (0, _dogmalang.list)(params.pkg);{
    const $aux15111791616788 = (((params != null ? params.target : null) != null ? (params != null ? params.target : null).os : null) != null ? ((params != null ? params.target : null) != null ? (params != null ? params.target : null).os : null).id : null) || _dogmalang.os.release().id;if ($aux15111791616788 == "alpine") {
      apk(params, ssh);
    } else if ($aux15111791616788 == "ubuntu") {
      apt(params, ssh);
    } else {
      _dogmalang.dogma.raise("OS not supported.");
    }
  }
}module.exports = exports = add;
function apk(params, ssh) {
  _dogmalang.dogma.paramExpected("params", params, null);{
    let cmd = "sudo apk add";if (params.allowUntrusted || params.allowUnauthenticated) {
      cmd += " --allow-untrusted";
    }cmd += " " + params.pkg.join(" ");(ssh || _dogmalang.exec)(cmd);
  }
}
function apt(params, ssh) {
  _dogmalang.dogma.paramExpected("params", params, null);{
    let cmd = "sudo apt install -q -y";if (params.reinstall) {
      cmd += " --reinstall";
    }if (params.allowUnauthenticated || params.allowUntrusted) {
      cmd += " --allow-unauthenticated";
    }cmd += " " + params.pkg.join(" ");cmd += " 2> /dev/null";(ssh || _dogmalang.exec)(cmd);
  }
}