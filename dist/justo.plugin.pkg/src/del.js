"use strict";

var _dogmalang = require("dogmalang");

function del(params, ssh) {
  _dogmalang.dogma.paramExpectedToHave("params", params, { pkg: { type: [_dogmalang.list, _dogmalang.text], mandatory: true } });_dogmalang.dogma.paramExpectedToBe("ssh", ssh, _dogmalang.func);params.pkg = (0, _dogmalang.list)(params.pkg);{
    const $aux15111791619666 = (((params != null ? params.target : null) != null ? (params != null ? params.target : null).os : null) != null ? ((params != null ? params.target : null) != null ? (params != null ? params.target : null).os : null).id : null) || _dogmalang.os.release().id;if ($aux15111791619666 == "alpine") {
      apk(params, ssh);
    } else if ($aux15111791619666 == "ubuntu") {
      apt(params, ssh);
    } else {
      _dogmalang.dogma.raise("OS not supported.");
    }
  }
}module.exports = exports = del;
function apk(params, ssh) {
  _dogmalang.dogma.paramExpected("params", params, null);{
    let cmd = "sudo apk del";cmd += " " + params.pkg.join(" ");(ssh || _dogmalang.exec)(cmd);
  }
}
function apt(params, ssh) {
  _dogmalang.dogma.paramExpected("params", params, null);{
    let cmd = "sudo apt" + (params.purge ? " purge" : " remove") + " -y";cmd += " " + params.pkg.join(" ");cmd += " 2> /dev/null";(ssh || _dogmalang.exec)(cmd);
  }
}