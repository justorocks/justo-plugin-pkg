"use strict";

var _dogmalang = require("dogmalang");

function update(params, ssh) {
  _dogmalang.dogma.paramExpected("params", params, null);_dogmalang.dogma.paramExpectedToBe("ssh", ssh, _dogmalang.func);{
    const $aux15111791619704 = (((params != null ? params.target : null) != null ? (params != null ? params.target : null).os : null) != null ? ((params != null ? params.target : null) != null ? (params != null ? params.target : null).os : null).id : null) || _dogmalang.os.release().id;if ($aux15111791619704 == "alpine") {
      apk(ssh);
    } else if ($aux15111791619704 == "ubuntu") {
      apt(ssh);
    } else {
      _dogmalang.dogma.raise("OS not supported.");
    }
  }
}module.exports = exports = update;
function apk(ssh) {
  {
    return (ssh || _dogmalang.exec)("sudo apk update");
  }
}
function apt(ssh) {
  {
    return (ssh || _dogmalang.exec)("sudo apt-get update");
  }
}