# justo.plugin.pkg

**Justo** plugin for OS package managers.
**APK** and **APT** supported.
All tasks are local and SSH-compatible.

*Developed in [Dogma](http://dogmalang.com), compiled to JavaScript.*

*Proudly made with ♥ in Valencia, Spain, EU.*

## Use

```
const pkg = require("justo.plugin.pkg");
```

## pkg.update task

This task updates the local index:

```
pkg.update()
```

## pkg.add and pkg.install task

These tasks install one or multiple packages:

```
pkg.add({pkg, allowUntrusted, allowUnauthenticated, reinstall})
pkg.install({pkg, allowUntrusted, allowUnauthenticated, reinstall})

```

- `pkg` (string or string[], required). Package(s) to install.
- `allowUntrusted` and `allowUnauthorized` (bool). Allow to install untrusted packages.
- `reinstall` (bool). Reinstall the package(s)? Only for **APT**.

Example:

```
pkg.add({pkg: "lua5.3"})
```

## pkg.installed task

This task checks if a package is installed:

```
pkg.installed({pkg}) : bool
```

- `pkg` (string, required). Package to check.

Example:

```
if (!pkg.installed({pkg: "lua5.3"})) {
  pkg.add({pkg: "lua5.3"});
}
```

## pkg.available task

This task checks if a package is available to install:

```
pkg.available({pkg}) : bool
```

- `pkg` (string, required). Package to check.

Example:

```
if (!pkg.installed({pkg: "lua5.3"}) && pkg.available({pkg: "lua5.3"})) {
  pkg.add({pkg: "lua5.3"})
}
```

## pkg.del and pkg.remove tasks

These tasks remove one or multiple packages:

```
pkg.del({pkg})
pkg.remove({pkg})
```

- `pkg` (string or string[], required). Package(s) to remove.

Example:

```
pkg.del({pkg: "lua5.3"});
```
